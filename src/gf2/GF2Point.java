package gf2;

import java.util.List;

/**
 * Created by Dmitriy on 27.11.2016.
 */
//класс "точки" для графика с накоплением."Точка" - это строка таблицы графика с накоплением (список из (x0,y0,y1 И т.д.) до количества графиков)
public class GF2Point {
    private List<String> points;

    public GF2Point(List<String> points) {
        this.points = points;
    }

    public List<String> getPoints() {
        return points;
    }

    public void setPoints(List<String> points) {
        this.points = points;
    }
}
