import gf1.GF1Pane;
import gf2.GF2Pane;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;
import kd.KDPane;

/**
 * Created by Dmitriy on 25.11.2016.
 */
public class Main extends Application {

    public static void main(String[] args) {
        Application.launch(Main.class, args);//запуск JavaFX приложения,вызывается метод start(),который ниже
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        GF1Pane gf1Pane = new GF1Pane();//создаём панель для обычного графика
        GF2Pane gf2Pane = new GF2Pane();//создаём панель для графика с накоплением
        KDPane kdPane = new KDPane();//создаём панель для круговой диаграммы

        Tab gf1Tab = new Tab("Обычный график");//создаём вкладку с названием
        gf1Tab.setClosable(false);//устанавливаем свойство незакрываемости вкладки
        gf1Tab.setOnSelectionChanged(event -> {
            gf1Tab.setContent(gf1Pane.getPane());//устанавливаем в качестве содержимого вкладки соответствующую панель
        });

        Tab gf2Tab = new Tab("График с накоплением");//создаём вкладку с названием
        gf2Tab.setClosable(false);//устанавливаем свойство незакрываемости вкладки
        gf2Tab.setOnSelectionChanged(event -> {
            gf2Tab.setContent(gf2Pane.getPane());//устанавливаем в качестве содержимого вкладки соответствующую панель
        });

        Tab kdTab = new Tab("Круговая диаграмма");//создаём вкладку с названием
        kdTab.setClosable(false);//устанавливаем свойство незакрываемости вкладки
        kdTab.setOnSelectionChanged(event -> {
            kdTab.setContent(kdPane.getPane());//устанавливаем в качестве содержимого вкладки соответствующую панель
        });

        TabPane tabPane = new TabPane(gf1Tab, gf2Tab, kdTab);//создаём панель вкладок и добавляем туда вкладки,которые описали выше

        Scene scene = new Scene(tabPane);//создаём сцену,в качестве корневой панели ставим панель вкладок,задаём размеры сцены
        primaryStage.setScene(scene);//добавляем сцену к окну
        primaryStage.setTitle("Деловая графика");//устанавливаем название окна
        primaryStage.setMaximized(true);//раскрываем окно во весь экран
        primaryStage.show();//показываем окно
    }
}
