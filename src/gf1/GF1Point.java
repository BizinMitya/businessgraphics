package gf1;

import java.util.List;

/**
 * Created by Dmitriy on 27.11.2016.
 */
//класс "точки" для обычного графика."Точка" - это строка таблицы обычного графика(список из (x0,y0),(x1,y1) И т.д. до количества графиков)
public class GF1Point {
    private List<String> points;

    public GF1Point(List<String> points) {
        this.points = points;
    }

    public List<String> getPoints() {
        return points;
    }

    public void setPoints(List<String> points) {
        this.points = points;
    }
}
