package gf1;

import graphic.Graphic;
import graphic.Point;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.util.Callback;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dmitriy on 25.11.2016.
 */
//класс панели для рисования обычного графика,этот класс создаст панель и добавит на неё таблицу значений,кнопку,поле ввода и холст
public class GF1Pane {
    private Pane pane;//панель,на которой всё будет
    private TextField textField;//поле ввода количества строк
    private ObservableList<GF1Point> list;//список "точек" в памяти,его размер равен числу строк в таблице
    private TableView<GF1Point> tableView;//таблица для ввода значений("точек")
    private ScrollPane scrollPane;//прокручивающаяся панель,куда добавится таблица,чтобы таблица могла иметь до 100 строк и не выходить за пределы окна
    private Button button;//кнопка для построения графика
    private Canvas canvas;//холст для рисования
    private Graphic graphic;//объект для рисования графиков
    private List<List<Point>> graphics;//список списков точек для каждого графика(размер graphics - кол-во графиков,размер graphics.get(i) - число точек для i-го графика)
    private List<Color> colors;//список цветов для каждого графика
    private int numberOfGraphics = 5;//число графиков

    //конструктор для создания панели обычного графика
    public GF1Pane() {
        pane = new Pane();//создаём панель pane
        list = FXCollections.observableArrayList();//создаём список "точек",пока что пустой

        tableView = new TableView<>();//создаём объект таблицы
        tableView.setEditable(true);//ставим таблице свойство редактирования таблицы

        for (int i = 0; i < numberOfGraphics; i++) {//в цкиле создаём столбцы для графиков(всего 2*numberOfGraphics столбцов(у каждого графика x и н))
            final int iter = 2 * i;//финальная переменная,чтобы можно было использовать её в анонимном класс
            TableColumn tableColumnX = new TableColumn("X" + i);//создаём столбец в таблице с названием Xi
            tableColumnX.setSortable(false);//ставим свойство несортируемости данных в столбце
            //устанавливаем стобцу обработчик,который отвечает за "вытаскивание" из объекта "точки" нужного поля,в данном случае это элемент списка(точка - список) iter
            tableColumnX.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<GF1Point, String>, ObservableValue<String>>() {//анонимный класс
                @Override
                public ObservableValue<String> call(TableColumn.CellDataFeatures<GF1Point, String> param) {
                    return new ReadOnlyObjectWrapper(param.getValue().getPoints().get(iter));//возвращаем элементс номером iter из списка
                }
            });
            tableColumnX.setCellFactory(TextFieldTableCell.forTableColumn());//устанавливаем обработчик для редактирования ячеек по умолчанию
            //т.е. ввод по Enter будет
            tableColumnX.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent>() {//устанавливаем обработчик,который будет вызываться,когда мы отредактируем ячейку и нажмём Enter
                @Override
                public void handle(TableColumn.CellEditEvent event) {
                    String newValue = event.getNewValue().toString();//новое значение,т.е. то,которое было в ячейке перед нажатием Enter
                    try {//блок проверки правильности числа в ячейке
                        Double.parseDouble(newValue);//если здесь не бросится исключение,то число корректное(типа double с точкой,а не запятой)
                        ((GF1Point) event.getTableView().getItems().get(event.getTablePosition().getRow())).getPoints().set(iter, newValue);//устанавливаем в ячейку введённое число
                    } catch (NumberFormatException e) {//если число было введено неверно(буквы,запятые,прочие цифры)
                        //т.к. мы отвечем здесь за "точку"(т.е. за строку),хотя редактируем только ячейку,то мы должны оставить не тронутые ячейки из трёх других столбцов
                        List<String> tempList = new ArrayList<>();//список точек
                        for (int k = 0; k < 2 * numberOfGraphics; k++) {
                            if (k == iter) {//если точка iter
                                tempList.add(k, "0");//то ставим на её место 0
                            } else {//иначе ставим то,что там было до этого
                                tempList.add(k, ((GF1Point) event.getTableView().getItems().get(event.getTablePosition().getRow())).getPoints().get(k));
                            }
                        }
                        event.getTableView().getItems().set(event.getTablePosition().getRow(), new GF1Point(tempList));//устанавливаем список(в памяти)
                        ((GF1Point) event.getTableView().getItems().get(event.getTablePosition().getRow())).getPoints().set(iter, "0");//устанавливаем значение по умолчанию(это 0) на место(iter) неправильного числа,это в таблице(на экране)
                    }
                }
            });
            TableColumn tableColumnY = new TableColumn("Y" + i);//создаём столбец в таблице с названием Yi
            tableColumnY.setSortable(false);//ставим свойство несортируемости данных в столбце
            //устанавливаем стобцу обработчик,который отвечает за "вытаскивание" из объекта "точки" нужного поля,в данном случае это элемент списка(точка - список) iter+1
            tableColumnY.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<GF1Point, String>, ObservableValue<String>>() {//анонимный класс
                @Override
                public ObservableValue<String> call(TableColumn.CellDataFeatures<GF1Point, String> param) {
                    return new ReadOnlyObjectWrapper(param.getValue().getPoints().get(iter + 1));//возвращаем элементс номером iter+1 из списка
                }
            });
            tableColumnY.setCellFactory(TextFieldTableCell.forTableColumn());//устанавливаем обработчик для редактирования ячеек по умолчанию
            //т.е. ввод по Enter будет
            tableColumnY.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent>() {//устанавливаем обработчик,который будет вызываться,когда мы отредактируем ячейку и нажмём Enter
                @Override
                public void handle(TableColumn.CellEditEvent event) {
                    String newValue = event.getNewValue().toString();//новое значение,т.е. то,которое было в ячейке перед нажатием Enter
                    try {//блок проверки правильности числа в ячейке
                        Double.parseDouble(newValue);//если здесь не бросится исключение,то число корректное(типа double с точкой,а не запятой)
                        ((GF1Point) event.getTableView().getItems().get(event.getTablePosition().getRow())).getPoints().set(iter + 1, newValue);//устанавливаем в ячейку введённое число
                    } catch (NumberFormatException e) {//если число было введено неверно(буквы,запятые,прочие цифры)
                        //т.к. мы отвечем здесь за "точку"(т.е. за строку),хотя редактируем только ячейку,то мы должны оставить не тронутые ячейки из трёх других столбцов
                        List<String> tempList = new ArrayList<>();//список точек
                        for (int k = 0; k < 2 * numberOfGraphics; k++) {
                            if (k == iter + 1) {//если точка iter
                                tempList.add(k, "0");//то ставим на её место 0
                            } else {//иначе ставим то,что там было до этого
                                tempList.add(k, ((GF1Point) event.getTableView().getItems().get(event.getTablePosition().getRow())).getPoints().get(k));
                            }
                        }
                        event.getTableView().getItems().set(event.getTablePosition().getRow(), new GF1Point(tempList));//устанавливаем список(в памяти)
                        ((GF1Point) event.getTableView().getItems().get(event.getTablePosition().getRow())).getPoints().set(iter + 1, "0");//устанавливаем значение по умолчанию(это 0) на место(iter+1) неправильного числа,это в таблице(на экране)
                    }
                }
            });
            tableView.getColumns().addAll(tableColumnX, tableColumnY);//добавляем в таблицу две колонки для каждого графика - x и y
        }

        tableView.setItems(list);//устанавливаем в таблицу наш список "точек"(он сейчас пустой,но по мере заполнения таблицы будет изменяться)

        scrollPane = new ScrollPane();//создаём панель прокручивания
        scrollPane.setContent(tableView);//устанавливаем в неё таблицу
        scrollPane.layoutXProperty().bind(pane.widthProperty().multiply(0.01));//привязываем координату x панели к 0.01 от ширины главной панели(делаем зависимость от размеров окна)
        scrollPane.layoutYProperty().bind(pane.heightProperty().multiply(0.15));//привязываем координату y панели к 0.15 от высоты главной панели(делаем зависимость от размеров окна)
        scrollPane.prefWidthProperty().bind(pane.widthProperty().multiply(0.23));//привязываем ширину панели к 0.23 от ширины главной панели(делаем зависимость от размеров окна)
        scrollPane.prefHeightProperty().bind(pane.heightProperty().multiply(0.6));//привязываем высоту панели к 0.6 от высоты главной панели(делаем зависимость от размеров окна)
        scrollPane.setFitToHeight(true);//устанавливаем сглаживание по высоте
        scrollPane.setFitToWidth(true);//устанавливаем сглаживание по ширине

        textField = new TextField("0");//создаём поле для ввода числа строк таблицы и ставим туда значение по умолчанию 0
        textField.layoutXProperty().bind(pane.widthProperty().multiply(0.01));//привязываем координату x текстового поля к 0.01 от ширины главной панели(делаем зависимость от размеров окна)
        textField.layoutYProperty().bind(pane.heightProperty().multiply(0.05));//привязываем координату y текстового поля к 0.05 от высоты главной панели(делаем зависимость от размеров окна)
        textField.prefWidthProperty().bind(pane.widthProperty().multiply(0.04));//привязываем ширину текстового поля к 0.04 от ширины главной панели(делаем зависимость от размеров окна)
        textField.prefHeightProperty().bind(pane.heightProperty().multiply(0.03));//привязываем высоту текстового поля к 0.03 от высоты главной панели(делаем зависимость от размеров окна)
        textField.textProperty().addListener(new ChangeTextFieldListener(list, textField));//устанавливаем обработчик события редактирования этого поля,класс обработчика описан ниже,классу нужен список "точек" и само поле для ввода

        canvas = new Canvas();//создаём холст

        graphics = new ArrayList<>();//создаём список графиков
        colors = new ArrayList<>();//создаём список цветов
        graphic = new Graphic(colors, 2, graphics, canvas);//создаём кдасс для рисования графиков,ему нужны список цветов для графиков,толщина линии,список графиков и холст

        canvas.layoutXProperty().bind(pane.widthProperty().multiply(0.25));//привязываем координату x холста к 0.25 от ширины главной панели(делаем зависимость от размеров окна)
        canvas.widthProperty().bind(pane.widthProperty().multiply(0.75));//привязываем ширину холста к 0.75 от ширины главной панели(делаем зависимость от размеров окна)
        canvas.heightProperty().bind(pane.heightProperty());//привязываем высоту холста к высоте главной панели(делаем зависимость от размеров окна)
        canvas.widthProperty().addListener(observable -> {//обработчик при изменении ширины холста(который изменяется с изменением ширины панели,привязали выше)
            graphic.clear();//при изменении ширины очищаем график
            graphic.drawAxes();//рисуем оси
            graphic.drawGraphic();//рисуем график
            graphic.drawLegend();//рисование легенды
        });
        canvas.heightProperty().addListener(observable -> {//обработчик при изменении высоты холста(который изменяется с изменением высоты панели,привязали выше)
            graphic.clear();//при изменении высоты очищаем график
            graphic.drawAxes();//рисуем оси
            graphic.drawGraphic();//рисуем график
            graphic.drawLegend();//рисование легенды
        });

        button = new Button("Построить");//создаём кнопку для построения графиков
        button.layoutXProperty().bind(pane.widthProperty().multiply(0.01));//привязываем координату x кнопки к 0.01 от ширины главной панели(делаем зависимость от размеров окна)
        button.layoutYProperty().bind(pane.heightProperty().multiply(0.9));//привязываем координату y кнопки к 0.9 от высоты главной панели(делаем зависимость от размеров окна)
        button.prefWidthProperty().bind(pane.widthProperty().multiply(0.1));//привязываем ширину кнопки к 0.1 от ширины главной панели(делаем зависимость от размеров окна)
        button.prefHeightProperty().bind(pane.heightProperty().multiply(0.05));//привязываем высоту кнопки к 0.05 от высоты главной панели(делаем зависимость от размеров окна)
        button.setOnMouseClicked(event -> {//обработчик нажатия на кнопку
            graphics.clear();//очищаем список графиков
            graphic.clear();//очищаем график
            colors.clear();//очищаем список цветов
            for (int i = 0; i < numberOfGraphics; i++) {
                graphics.add(new ArrayList<>());//заполняем список графиков списками точек
                colors.add(new Color(Math.random(), Math.random(), Math.random(), 1));//заполняем список цветов случайными цветами
            }
            for (GF1Point gf1Point : tableView.getItems()) {//цикл по всем точкам в таблице
                for (int i = 0, t = 0; i < gf1Point.getPoints().size(); i += 2, t++) {
                    graphics.get(t).add(new Point(Double.parseDouble(gf1Point.getPoints().get(i)), Double.parseDouble(gf1Point.getPoints().get(i + 1))));//вытаскиваем значения из таблицы и записываем их в нужный график
                }
            }
            graphic.drawAxes();//рисуем оси
            graphic.drawGraphic();//рисуем график
            graphic.drawLegend();//рисование легенды
        });

        pane.getChildren().addAll(textField, scrollPane, tableView, button, canvas);//добавляем к панели все объекты,созданные выше
    }


    public Pane getPane() {
        return pane;
    }

    //вложенный класс обработчика события редактирования текстового поля
    public class ChangeTextFieldListener implements ChangeListener<String> {
        private ObservableList<GF1Point> list;//список "точек" в памяти
        private TextField textField;//текстовое поле

        public ChangeTextFieldListener(ObservableList<GF1Point> list, TextField textField) {
            this.list = list;
            this.textField = textField;
        }

        @Override
        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {//метод обработчика
            if (!newValue.equals("")) {//если новое значение не пусто
                if (newValue.matches("^[\\d]+$") && newValue.length() <= 2) {//проверяем регулярным выражением,что новое значение - это одна или две цифры
                    if (Integer.parseInt(newValue) >= (oldValue.equals("") ? 0 : Integer.parseInt(oldValue))) {//если новое знаечние больше или равно старому,то надо добавить строки(старое может быть пустым,тогда считаем,что число строк было 0)
                        for (int i = (oldValue.equals("") ? 0 : Integer.parseInt(oldValue)); i < Integer.parseInt(newValue); i++) {//в цикле добавляем строки("точки") со значениями по умолчанию 0
                            List<String> tempList = new ArrayList<>();
                            for (int j = 0; j < 2 * numberOfGraphics; j++) {
                                tempList.add("0");
                            }
                            list.add(i, new GF1Point(tempList));
                        }
                    } else {//если наоборот старое значение было больше нового,то нужно удалить строки(старое вновь может быть пустым,тогда считаем,что это 0)
                        list.remove(Integer.parseInt(newValue), (oldValue.equals("") ? 0 : Integer.parseInt(oldValue)));
                    }
                } else {//иначе,если было введено не число
                    textField.textProperty().removeListener(this);//отключаем на время обработчик
                    textField.setText(oldValue);//чтобы на этой строке он заново не запустился,т.к. метод setText() его вызывает,а сейчас он не вызовется
                    textField.textProperty().addListener(this);//устанавливаем опять этот обработчик
                }
            } else {//если новое значение пусто,то очищаем всю таблицу
                list.clear();
            }
        }
    }

}
