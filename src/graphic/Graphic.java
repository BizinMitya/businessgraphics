package graphic;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontSmoothingType;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Dmitriy on 25.11.2016.
 */
//класс рисования графиков
public class Graphic {
    private List<Color> colors;//список цветов
    private double lineWidth;//толщина линии
    private List<List<Point>> graphics;//список списков точек для каждого графика(размер graphics - кол-во графиков,размер graphics.get(i) - число точек для i-го графика)
    private Canvas canvas;//холст

    public Graphic(List<Color> colors, double lineWidth, List<List<Point>> graphics, Canvas canvas) {
        this.canvas = canvas;
        this.colors = colors;
        if (colors.size() == 0) {//если цветов 0,то в цикле заполняем весь список синими цветами
            for (int i = 0; i < graphics.size(); i++) {
                colors.add(Color.BLUE);
            }
        }
        if (colors.size() < graphics.size()) {//если цветов меньше,чем точек,то аналогично заполняем оставшиеся цвета последним цветом списка
            for (int i = 0; i < graphics.size() - colors.size(); i++) {
                colors.add(colors.get(colors.size() - 1));
            }
        }
        this.lineWidth = lineWidth;
        this.graphics = graphics;
    }

    //метод рисования графиков
    public void drawGraphic() {
        if (graphics.size() > 0 && graphics.get(1).size() > 0) {//если графиков больше 0 и точек первого графика больше 0,то рисуем
            GraphicsContext graphicsContext = canvas.getGraphicsContext2D();//создаём контекст для рисования
            graphicsContext.setLineWidth(lineWidth);//устанавливаем толщину линии
            graphicsContext.setFontSmoothingType(FontSmoothingType.GRAY);//устанавливаем сглаживание шрифтов
            double width = canvas.getWidth();//получаем текущую ширину холста
            double height = canvas.getHeight();//получаем текущую высоту холста
            double maxX = Math.abs(graphics.get(0).get(0).getX());//для масштабирования графиков нжно найти максимум по x и y,пока ставим макисмум по x - x первой точки первого графика
            double maxY = Math.abs(graphics.get(0).get(0).getY());//пока ставим макисмум по y - y первой точки первого графика
            for (List<Point> points : graphics) {//цикл по всем графикам,на каждой итерации - список точек тещущего графика
                if (points.size() > 0) {
                    points.sort((o1, o2) -> Double.compare(o1.getX(), o2.getX()));//сортируем точки по x(лямбда-выражение с компаратором)
                    if (Math.abs(points.get(points.size() - 1).getX()) > maxX) {//если x последней точки отсортированного списка больше по модулю текущего максимума
                        maxX = Math.abs(points.get(points.size() - 1).getX());//то это новый максимум
                    }
                    if (Math.abs(points.get(0).getX()) > maxX) {//если x первой точки(отрицательное число по модулю может быть больше положительного,поэтому с краю проверяем) отсортированного списка больше по модулю текущего максимума
                        maxX = Math.abs(points.get(0).getX());//то это новый максимум
                    }
                    double tempMax = points.stream().max((o1, o2) -> Double.compare(o1.getY(), o2.getY())).get().getY();//текущий максимум y (лямбда-выражение,с компаратором)
                    double tempMin = points.stream().min((o1, o2) -> Double.compare(o1.getY(), o2.getY())).get().getY();//текущий минимум y (лямбда-выражение,с компаратором)
                    if (Math.abs(tempMax) > maxY) {//рассуждения,аналогичные выше
                        maxY = Math.abs(tempMax);
                    }
                    if (Math.abs(tempMin) > maxY) {
                        maxY = Math.abs(tempMin);
                    }
                }
            }
            double scaleX = maxX == 0 ? 1 : 0.95 * (width / (2 * maxX));//вычисляем масштаб по x как 0.95 от отношения ширины холста к 2 максимумам по x.Если maxX=0,то без масштаба(равен 1)
            double scaleY = maxY == 0 ? 1 : 0.95 * (height / (2 * maxY));//вычисляем масштаб по y как 0.95 от отношения высоты холста к 2 максимумам по y.Если maxY=0,то без масштаба(равен 1)
            Set<Double> setX = new HashSet<>();//создаём множество(не содержит одинаковых элементов,чтобы не красить больше одного раза одинаковые точки) точек x
            Set<Double> setY = new HashSet<>();//создаём множество точек y
            for (int i = 0; i < graphics.size(); i++) {//в цкиле по всем графикам
                graphicsContext.setStroke(colors.get(i));//ставим цвет линии на текущий цвет графика
                graphicsContext.setFill(colors.get(i));//ставим заполнение цветом(для точек) на текщций цвет графика
                List<Point> points = graphics.get(i);//получаем список точек текущего графика
                double x[] = new double[points.size()];//массив отмасштабированных точек x
                for (int j = 0; j < x.length; j++) {
                    x[j] = scaleX * points.get(j).getX() + width / 2;//каждую точку умножаем на масштаб по x и центрируем на начало координат (width/2,height/2)
                }
                double y[] = new double[points.size()];//массив отмасштабированных точек y
                for (int j = 0; j < y.length; j++) {
                    y[j] = -scaleY * points.get(j).getY() + height / 2;//каждую точку умножаем на МИНУС масштаб по y (минус,т.к. у холста отсчёт идёт от левого верхнего угла,т.е. ось OY направлена ВНИЗ) и центрируем на начало координат (width/2,height/2)
                }
                graphicsContext.strokePolyline(x, y, points.size());//прводим линию через эти точки
                for (int j = 0; j < points.size(); j++) {//цил для рисования точек
                    graphicsContext.fillOval(x[j] - 2 * lineWidth, y[j] - 2 * lineWidth, 4 * lineWidth, 4 * lineWidth);//рисуем точки с радиусом 4 толщины линии(при рисовании круга отсчёт идёт от левого верхнего угла квадрата,описанного вокруг этого круга,поэтому центрируем,отнимая половину радиуса от x и y)
                }
                graphicsContext.setStroke(Color.BLACK);//ставим цвет линии чёрный
                graphicsContext.setFill(Color.BLACK);//ставим чёрное заполнение
                graphicsContext.setLineWidth(1);//ставим толщину линии 1
                graphicsContext.setFont(new Font("Arial", 9));//устанавливаем шрифт и размер шрифта
                for (int j = 0; j < points.size(); j++) {//цикл по точкам x
                    if (x[j] != width / 2) {//не рисуем точку 0(она и так есть)
                        if (!setX.contains(x[j])) {//если точки нет во множестве,то
                            graphicsContext.strokeLine(x[j], height / 2 - 5, x[j], height / 2 + 5);//рисуем засечку на оси x
                            graphicsContext.fillText(String.valueOf(points.get(j).getX()), x[j] - 10, height / 2 + 20);//и подписываем её значением
                            setX.add(x[j]);//добавляем во множество и в следующий раз сюда уже не зайдём
                        }
                    }
                }
                for (int j = 0; j < points.size(); j++) {//цикл по точкам y
                    if (y[j] != height / 2) {//не рисуем точку 0(она и так есть)
                        if (!setY.contains(y[j])) {//если точки нет во множестве,то
                            graphicsContext.strokeLine(width / 2 - 5, y[j], width / 2 + 5, y[j]);//рисуем засечку на оси y
                            graphicsContext.fillText(String.valueOf(points.get(j).getY()), width / 2 + 10, y[j] + 5);//и подписываем её значением
                            setY.add(y[j]);//добавляем во множество и в следующий раз сюда уже не зайдём
                        }
                    }
                }
            }
        }
    }

    //метод рисования осей
    public void drawAxes() {
        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();//получаем контекст для рисования
        double width = canvas.getWidth();//получаем текущую ширину холста
        double height = canvas.getHeight();//получаем текущую высоту холста
        graphicsContext.setFont(new Font("Arial", 9));//ставим шрифт и размер шрифта
        graphicsContext.setFontSmoothingType(FontSmoothingType.GRAY);//устанавливаем сглаживание шрифтов
        graphicsContext.setStroke(Color.BLACK);//ставив цвет линии чёрный
        graphicsContext.setFill(Color.BLACK);//ставим чёрное заполнение
        graphicsContext.setLineWidth(1);//ставим толщину линии 1
        graphicsContext.strokeLine(width / 2, 0, width / 2, height);//проводим ось Y
        graphicsContext.strokeLine(0, height / 2, width, height / 2);//проводим ось X
        graphicsContext.strokeLine(width, height / 2, width - 10, height / 2 - 10 * Math.sqrt(2) / 2);//рисуем стрелку на оси X
        graphicsContext.strokeLine(width, height / 2, width - 10, height / 2 + 10 * Math.sqrt(2) / 2);//рисуем стрелку на оси X
        graphicsContext.fillText("X", width - 10, height / 2 - 20);//рисуем букву X
        graphicsContext.strokeLine(width / 2, 0, width / 2 - 10 * Math.sqrt(2) / 2, 10);//рисуем стрелку на оси Y
        graphicsContext.strokeLine(width / 2, 0, width / 2 + 10 * Math.sqrt(2) / 2, 10);//рисуем стрелку на оси Y
        graphicsContext.fillText("Y", width / 2 + 10, 10);//рисуем букву Y
        graphicsContext.fillText("0", width / 2 + 5, height / 2 - 5);//рисуем цифру 0
    }

    //метод очистки холста
    public void clear() {
        double width = canvas.getWidth();
        double height = canvas.getHeight();
        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        graphicsContext.clearRect(0, 0, width, height);//очищаем по всей ширине и высоте холста
    }

    //метод рисования легенды
    public void drawLegend() {
        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();//получаем контекст для рисования
        double width = canvas.getWidth();//получаем ширину холста
        double height = canvas.getHeight();//получаем высоту холста
        graphicsContext.setStroke(Color.BLACK);//ставим цвет линии чёрный
        graphicsContext.setLineWidth(1);//ставим толщину линии
        graphicsContext.setFont(new Font("Arial", 9));//ставим шрифт и размер шрифта
        graphicsContext.setFontSmoothingType(FontSmoothingType.LCD);//устанавливаем сглаживание шрифтов
        graphicsContext.strokeRoundRect(0.75 * width, 0.75 * height, 0.2 * width, (graphics.size() + 1) * 0.02 * height, 20, 20);//рисуем прямоугольную рамку
        for (int i = 0; i < graphics.size(); i++) {//цикл по числу графиков
            graphicsContext.setLineWidth(3);//толщину линии ставим 3
            graphicsContext.setStroke(colors.get(i));//ставим цвет текущей линии
            graphicsContext.strokeLine(0.75 * width + 0.01 * width, 0.75 * height + (i + 1) * 0.02 * height, 0.75 * width + 0.1 * width, 0.75 * height + (i + 1) * 0.02 * height);//проводим цветную линию
            graphicsContext.fillText("График " + (i + 1), 0.75 * width + 0.11 * width, graphicsContext.getFont().getSize() / 2 + 0.75 * height + (i + 1) * 0.02 * height);//пишем название графика
        }
    }

    public void setColors(List<Color> colors) {
        this.colors = colors;
    }

    public void setLineWidth(double lineWidth) {
        this.lineWidth = lineWidth;
    }

    public void setGraphics(List<List<Point>> graphics) {
        this.graphics = graphics;
    }

    public void setCanvas(Canvas canvas) {
        this.canvas = canvas;
    }
}
