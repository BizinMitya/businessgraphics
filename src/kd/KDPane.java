package kd;

import chart.PieChart;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dmitriy on 25.11.2016.
 */
public class KDPane {
    private Pane pane;//панель,на которой всё будет
    private Canvas canvas;//холст для рисования
    private PieChart pieChart;//объект для рисования круговой диаграммы
    private List<KDPoint> kdPoints;//список точек для рисования круговой диаграммы
    private List<Color> colors;//список цветов для каждого сектора(точки) диаграммы
    private TextField textField;//поле ввода количества строк
    private ObservableList<KDPoint> list;//список точек в памяти,его размер равен числу строк в таблице
    private TableView<KDPoint> tableView;//таблица для ввода значений(точек)
    private ScrollPane scrollPane;//прокручивающаяся панель,куда добавится таблица,чтобы таблица могла иметь до 100 строк и не выходить за пределы окна
    private Button button;//кнопка для построения диаграммы

    public KDPane() {
        pane = new Pane();//создаём панель pane
        list = FXCollections.observableArrayList();//создаём список "точек",пока что пустой

        tableView = new TableView<>();//создаём объект таблицы
        tableView.setEditable(true);//ставим таблице свойство редактирования таблицы
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);//политика выравнивания ширины столбцов таким образом,чтобы все они вмещались в ширину самой таблицы

        TableColumn tableColumnName = new TableColumn("Название");//создаём столбец в таблице с названием "Название" :)
        tableColumnName.setSortable(false);//ставим свойство несортируемости данных в столбце
        //устанавливаем стобцу обработчик,который отвечает за "вытаскивание" из объекта точки нужного поля,в данном случае это x
        //т.е. имеем соответствие точка <-> поле name
        tableColumnName.setCellValueFactory(new PropertyValueFactory<KDPoint, String>("name"));
        tableColumnName.setCellFactory(TextFieldTableCell.forTableColumn());//устанавливаем обработчик для редактирования ячеек по умолчанию
        //т.е. ввод по Enter будет
        tableColumnName.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent>() {//устанавливаем обработчик,который будет вызываться,когда мы отредактируем ячейку и нажмём Enter
            @Override
            public void handle(TableColumn.CellEditEvent event) {
                //этот столбец отвечает за название сектора на диграмме,поэтому нет смысла проверять его на корректность
                //поэтому просто устанавливаем введённое значение в ячейку таблицы
                ((KDPoint) event.getTableView().getItems().get(event.getTablePosition().getRow())).setName(event.getNewValue().toString());
            }
        });
        TableColumn tableColumnValue = new TableColumn("Значение");//создаём столбец в таблице с названием "Значение"
        tableColumnValue.setSortable(false);//ставим свойство несортируемости данных в столбце
        //устанавливаем стобцу обработчик,который отвечает за "вытаскивание" из объекта точки нужного поля,в данном случае это y
        //т.е. имеем соответствие точка <-> поле value
        tableColumnValue.setCellValueFactory(new PropertyValueFactory<KDPoint, String>("value"));
        tableColumnValue.setCellFactory(TextFieldTableCell.forTableColumn());//устанавливаем обработчик для редактирования ячеек по умолчанию
        //т.е. ввод по Enter будет
        tableColumnValue.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent>() {
            @Override
            public void handle(TableColumn.CellEditEvent event) {
                String newValue = event.getNewValue().toString();//новое значение,т.е. то,которое было в ячейке перед нажатием Enter
                try {//блок проверки правильности числа в ячейке
                    double d = Double.parseDouble(newValue);//если здесь не бросится исключение,то число корректное(типа double с точкой,а не запятой)
                    if (d < 0) throw new NumberFormatException();//если число отрицательное,то бросаем исключение
                    ((KDPoint) event.getTableView().getItems().get(event.getTablePosition().getRow())).setValue(newValue);//устанавливаем в ячейку введённое число
                } catch (NumberFormatException e) {//если число было введено неверно(буквы,запятые,прочие цифры,отрицательное число)
                    //т.к. мы отвечем здесь за точку(т.е. за строку),хотя редактируем только ячейку,то мы должны оставить не тронутые ячейки из двух других столбцов
                    String name = ((KDPoint) event.getTableView().getItems().get(event.getTablePosition().getRow())).getName();//вытаскиваем из столбца name значение name этой точки(строки)
                    event.getTableView().getItems().set(event.getTablePosition().getRow(), new KDPoint(name, "0"));//устанавливаем значение по умолчанию(это 0) на место(value) неправильного числа,это в списке(в памяти)
                    ((KDPoint) event.getTableView().getItems().get(event.getTablePosition().getRow())).setValue("0");//устанавливаем значение по умолчанию(это 0) на место(value) неправильного числа,это в таблице(на экране)
                }
            }
        });

        tableView.setItems(list);//устанавливаем в таблицу наш список "точек"(он сейчас пустой,но по мере заполнения таблицы будет изменяться)
        tableView.getColumns().addAll(tableColumnName, tableColumnValue);//добавляем в таблицу три колонки

        scrollPane = new ScrollPane();//создаём панель прокручивания
        scrollPane.setContent(tableView);//устанавливаем в неё таблицу
        scrollPane.layoutXProperty().bind(pane.widthProperty().multiply(0.01));//привязываем координату x панели к 0.01 от ширины главной панели(делаем зависимость от размеров окна)
        scrollPane.layoutYProperty().bind(pane.heightProperty().multiply(0.15));//привязываем координату y панели к 0.15 от высоты главной панели(делаем зависимость от размеров окна)
        scrollPane.prefWidthProperty().bind(pane.widthProperty().multiply(0.23));//привязываем ширину панели к 0.23 от ширины главной панели(делаем зависимость от размеров окна)
        scrollPane.prefHeightProperty().bind(pane.heightProperty().multiply(0.6));//привязываем высоту панели к 0.6 от высоты главной панели(делаем зависимость от размеров окна)
        scrollPane.setFitToHeight(true);//устанавливаем сглаживание по высоте
        scrollPane.setFitToWidth(true);//устанавливаем сглаживание по ширине

        textField = new TextField("0");//создаём поле для ввода числа строк таблицы и ставим туда значение по умолчанию 0
        textField.layoutXProperty().bind(pane.widthProperty().multiply(0.01));//привязываем координату x текстового поля к 0.01 от ширины главной панели(делаем зависимость от размеров окна)
        textField.layoutYProperty().bind(pane.heightProperty().multiply(0.05));//привязываем координату y текстового поля к 0.05 от высоты главной панели(делаем зависимость от размеров окна)
        textField.prefWidthProperty().bind(pane.widthProperty().multiply(0.04));//привязываем ширину текстового поля к 0.04 от ширины главной панели(делаем зависимость от размеров окна)
        textField.prefHeightProperty().bind(pane.heightProperty().multiply(0.03));//привязываем высоту текстового поля к 0.03 от высоты главной панели(делаем зависимость от размеров окна)
        textField.textProperty().addListener(new KDPane.ChangeTextFieldListener(list, textField));//устанавливаем обработчик события редактирования этого поля,класс обработчика описан ниже,классу нужен список "точек" и само поле для ввода

        canvas = new Canvas();//создаём холст для рисования

        kdPoints = new ArrayList<>();//создаём список точек для круговой диаграммы
        colors = new ArrayList<>();//создаём список цветов секторов
        pieChart = new PieChart(colors, kdPoints, canvas);//создаём объект для рисования круговой диаграммы

        canvas.layoutXProperty().bind(pane.widthProperty().multiply(0.25));//привязываем координату x холста к 0.25 от ширины главной панели(делаем зависимость от размеров окна)
        canvas.widthProperty().bind(pane.widthProperty().multiply(0.75));//привязываем ширину холста к 0.75 от ширины главной панели(делаем зависимость от размеров окна)
        canvas.heightProperty().bind(pane.heightProperty());//привязываем высоту холста к высоте главной панели(делаем зависимость от размеров окна)
        canvas.widthProperty().addListener(observable -> {//обработчик при изменении ширины холста(который изменяется с изменением ширины панели,привязали выше)
            pieChart.clear();//очищаем диаграмму
            pieChart.drawChart();//рисуем заново
            pieChart.drawLegend();//рисуем легенду
        });
        canvas.heightProperty().addListener(observable -> {//обработчик при изменении высоты холста(который изменяется с изменением высоты панели,привязали выше)
            pieChart.clear();//очищаем диаграмму
            pieChart.drawChart();//рисуем заново
            pieChart.drawLegend();//рисуем легенду
        });

        button = new Button("Построить");//создаём кнопку для построения графиков
        button.layoutXProperty().bind(pane.widthProperty().multiply(0.01));//привязываем координату x кнопки к 0.01 от ширины главной панели(делаем зависимость от размеров окна)
        button.layoutYProperty().bind(pane.heightProperty().multiply(0.9));//привязываем координату y кнопки к 0.9 от высоты главной панели(делаем зависимость от размеров окна)
        button.prefWidthProperty().bind(pane.widthProperty().multiply(0.1));//привязываем ширину кнопки к 0.1 от ширины главной панели(делаем зависимость от размеров окна)
        button.prefHeightProperty().bind(pane.heightProperty().multiply(0.05));//привязываем высоту кнопки к 0.05 от высоты главной панели(делаем зависимость от размеров окна)
        button.setOnMouseClicked(event -> {//обработчик нажатия на кнопку
            kdPoints.clear();//очищаем список точек для диаграммы
            colors.clear();//очищаем список цветов
            for (KDPoint kdPoint : tableView.getItems()) {//цикл по всем точкам в таблице
                kdPoints.add(new KDPoint(kdPoint.getName(), kdPoint.getValue()));//добавляем в список точки из таблицы
                colors.add(new Color(Math.random(), Math.random(), Math.random(), 1));//в список цветов добавляем случайный цвет с прозрачностью 1
            }
            pieChart.clear();//очищаем диаграмму
            pieChart.drawChart();//рисуем новую
            pieChart.drawLegend();//рисуем легенду
        });

        pane.getChildren().addAll(textField, scrollPane, tableView, button, canvas);//добавляем к панели все объекты,созданные выше
    }

    public Pane getPane() {
        return pane;
    }

    //вложенный класс обработчика события редактирования текстового поля
    public class ChangeTextFieldListener implements ChangeListener<String> {
        private ObservableList<KDPoint> list;//список точек в памяти
        private TextField textField;//текстовое поле

        public ChangeTextFieldListener(ObservableList<KDPoint> list, TextField textField) {
            this.list = list;
            this.textField = textField;
        }

        @Override
        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {//метод обработичка
            if (!newValue.equals("")) {//если новое значение не пусто
                if (newValue.matches("^[\\d]+$") && newValue.length() <= 2) {//проверем регулярным выражением,что новое значение - это одна или две цифры
                    if (Integer.parseInt(newValue) >= (oldValue.equals("") ? 0 : Integer.parseInt(oldValue))) {//если новое знаечние больше или равно старому,то надо добавить строки(старое может быть пустым,тогда считаем,что число строк было 0)
                        for (int i = (oldValue.equals("") ? 0 : Integer.parseInt(oldValue)); i < Integer.parseInt(newValue); i++) {//в цикле добавляем строки(точки) со значениями по умолчанию - пустая строка и 0
                            list.add(i, new KDPoint("", "0"));
                        }
                    } else {//если наоборот старое значение было больше нового,то нужно удалить строки(старое вновь может быть пустым,тогда считаем,что это 0)
                        list.remove(Integer.parseInt(newValue), (oldValue.equals("") ? 0 : Integer.parseInt(oldValue)));
                    }
                } else {//иначе,если было введено не число
                    textField.textProperty().removeListener(this);//отключаем на время обработчик
                    textField.setText(oldValue);//чтобы на этой строке он заново не запустился,т.к. метод setText() его вызывает,а сейчас он не вызовется
                    textField.textProperty().addListener(this);//устанавливаем опять этот обработчик
                }
            } else {//если новое значение пусто,то очищаем всю таблицу
                list.clear();
            }
        }
    }
}
